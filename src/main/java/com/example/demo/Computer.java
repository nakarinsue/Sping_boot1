package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Computer {

    public Computer(String name, long shopId) {
        this.name = name;
        this.shopId = shopId;
    }

    public  Computer() {}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Column(name="ID")
    private long id;

    @Getter @Setter
    @JsonProperty
    @Column(name="NAME")
    private String name;

    @Getter @Setter
    @JsonProperty
    @Column(name="SHOPID")
    private long shopId;

    @ManyToOne
    @Getter @Setter
    @JsonIgnore
    @JoinColumn(name="SHOPID", insertable = false, updatable = false)
    private Shop shop;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
