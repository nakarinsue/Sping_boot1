package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/computer")
public class ComputerController {

    @Autowired
    private ComputerRepository computerRepository;

    @GetMapping("/get")
    public HttpEntity<List<Computer>> getAll() {
        List<Computer> computers =  computerRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(computers);
    }

    @PostMapping("/add")
    public HttpEntity<String> addComputer(@RequestBody Computer computer) {
        computerRepository.save(computer);
        return ResponseEntity.status(HttpStatus.OK).body("Save success");
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<String> editComputer(@PathVariable long id,
                                       @RequestParam Optional<String> name,
                                       @RequestParam Optional<Long> shopId) {

        Optional<Computer> computer = computerRepository.findById(id);

        if (computer.isPresent()) {
            if (name.isPresent()) computer.get().setName(name.get());
            if (shopId.isPresent()) computer.get().setShopId(shopId.get());
        }

        computerRepository.save(computer.get());

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("แก้ไขสำเร็จ"));
    }

    @DeleteMapping(path = "/delete/{id}")
    public HttpEntity<String> delete(@PathVariable long id) {
        Optional<Computer> computer = computerRepository.findById(id);

        if (computer.isPresent()) {
            computerRepository.deleteById(id);
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.valueOf("ไม่มีไอ้สัส"));
        }

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("ลบ id " + String.valueOf(id) + " สำเร็จ"));
    }
}