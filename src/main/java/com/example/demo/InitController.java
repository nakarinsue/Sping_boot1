package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class InitController {

    @Autowired
    ShopController shopController;

    @Autowired
    StaffController staffController;

    @Autowired
    ComputerController computerController;

    @GetMapping("/init")
    public void init(){
        // init code goes here
        HttpEntity<Shop> shop1 = shopController.addShop("Pak kret");
        HttpEntity<Shop> shop2 = shopController.addShop("Don Mueng");
        HttpEntity<Shop> shop3 = shopController.addShop("Lak Si");

        HttpEntity<Staff> staff1 = staffController.addStaff(new Staff("Boat","MANAGER",shop1.getBody().getId()));
        HttpEntity<Staff> staff2 = staffController.addStaff(new Staff("Save","MANAGER",shop2.getBody().getId()));
        HttpEntity<Staff> staff3 = staffController.addStaff(new Staff("Jame","MANAGER",shop3.getBody().getId()));

        shopController.addManager(shop1.getBody().getId(), staff1.getBody());
        shopController.addManager(shop2.getBody().getId(), staff2.getBody());
        shopController.addManager(shop3.getBody().getId(), staff3.getBody());

        staffController.addStaff(new Staff("Abby","TECHNICIAN", shop1.getBody().getId()));
        staffController.addStaff(new Staff("Thomas","JANITOR", shop1.getBody().getId()));
        staffController.addStaff(new Staff("JOHN","GARDENER", shop1.getBody().getId()));

        staffController.addStaff(new Staff("Celene","TECHNICIAN", shop2.getBody().getId()));
        staffController.addStaff(new Staff("Joseph","JANITOR", shop2.getBody().getId()));
        staffController.addStaff(new Staff("Robert","GARDENER", shop2.getBody().getId()));

        staffController.addStaff(new Staff("Katy","TECHNICIAN", shop3.getBody().getId()));
        staffController.addStaff(new Staff("Taylor","JANITOR", shop3.getBody().getId()));
        staffController.addStaff(new Staff("Michael","GARDENER", shop3.getBody().getId()));

        computerController.addComputer(new Computer("Dell PX500",shop1.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX600",shop1.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX500",shop1.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX700",shop1.getBody().getId()));
        computerController.addComputer(new Computer("Acer A50",shop1.getBody().getId()));

        computerController.addComputer(new Computer("Dell PX100",shop2.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX600",shop2.getBody().getId()));
        computerController.addComputer(new Computer("Huawei PX500",shop2.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX700",shop2.getBody().getId()));
        computerController.addComputer(new Computer("Acer A50",shop2.getBody().getId()));

        computerController.addComputer(new Computer("PRX PX100",shop3.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX600",shop3.getBody().getId()));
        computerController.addComputer(new Computer("Huawei PX500",shop3.getBody().getId()));
        computerController.addComputer(new Computer("Dell PX700",shop3.getBody().getId()));
        computerController.addComputer(new Computer("Acer A50",shop3.getBody().getId()));
    }
}
