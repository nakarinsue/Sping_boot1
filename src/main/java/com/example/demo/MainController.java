package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

	@Autowired
    private PersonRepository personRepository;

	@GetMapping(path = "test")
    public List<Person> getAllPerson() {
	    return personRepository.findAll();
    }

    @GetMapping(path = "test2")
    public HttpEntity<String> testAll() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("bad");
    }

    @DeleteMapping(path = "/delete/{id}")
    public HttpEntity<String> delete(@PathVariable long id) {
	    Optional<Person> person = personRepository.findById(id);

	    if (person.isPresent()) {
            personRepository.deleteById(id);
        }
	    else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.valueOf("ไม่มีไอ้สัส"));
        }

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("ลบ id " + String.valueOf(id) + " สำเร็จ"));

    }

    @PostMapping(path = "/add")
    public HttpEntity<String> add(@RequestBody Person person) {
        personRepository.save(person);
        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("เพิ่มสำเร็จ"));
    }

    @PatchMapping(path = "/edit/{id}")
    public HttpEntity<String> add(@PathVariable long id,
                                  @RequestParam Optional<String> firstName,
                                  @RequestParam Optional<String> lastName) {

	    Optional<Person> person = personRepository.findById(id);

	    if (person.isPresent()) {
	        if (firstName.isPresent()) person.get().setFirstName(firstName.get());
            if (lastName.isPresent()) person.get().setLastName(lastName.get());
        }

        personRepository.save(person.get());

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("แก้ไขสำเร็จ"));
    }


}
