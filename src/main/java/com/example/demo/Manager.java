package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class Manager {

    public Manager(long staffId, long shopId) {
        this.staffId = staffId;
        this.shopId = shopId;
    }

    public  Manager() {}
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private long id;

    @Getter
    @Setter
    @JsonProperty
    @Column(name="STAFFID")
    private long staffId;

    @Getter @Setter @JsonProperty
    @Column(name="SHOPID")
    private long shopId;


    public long getStaffId() {
        return staffId;
    }

    public void setStaffId(long staffId) {
        this.staffId = staffId;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

}
