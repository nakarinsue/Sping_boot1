package com.example.demo;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	private long id;

	@Getter @Setter @JsonProperty
	private String firstName;
	
	@Getter @Setter @JsonProperty
	private String lastName;

	protected Person() {}
	
	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
    public String toString() {
		/*
		 * return String.format( "Person[id=%d, firstName='%s', lastName='%s']", id,
		 * firstName, lastName);
		 */
        return String.format(
                "%d = %s %s",
                id, firstName, lastName);
    }
}
