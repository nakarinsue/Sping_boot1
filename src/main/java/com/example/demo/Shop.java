package com.example.demo;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;

@Entity
public class Shop {

    public  Shop() {}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Column(name="ID")
    private long id;

    @Getter @Setter @JsonProperty
    @Column(name="NAME")
    private String name;

    @Getter @Setter @JsonProperty
    @OneToMany(mappedBy = "shop")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Staff> staffs;

    @Getter @Setter @JsonProperty
    @OneToMany(mappedBy = "shop")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Computer> computers;

    @Getter @Setter @JsonProperty
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(cascade = {CascadeType.ALL})
    private Manager manager;

    public Shop(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Staff> getStaffs() {
        return staffs;
    }

    public void setStaffs(List<Staff> staffs) {
        this.staffs = staffs;
    }

    public List<Computer> getComputers() {
        return computers;
    }

    public void setComputers(List<Computer> computers) {
        this.computers = computers;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }
}
