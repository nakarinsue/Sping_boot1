package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private  StaffRepository staffRepository;

    @GetMapping("/get")
    public HttpEntity<List<Shop>> getAll() {
        List<Shop> shops = shopRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(shops);
    }

    @PostMapping("/add")
    public HttpEntity<Shop> addShop(@RequestParam String shopName) {
        Shop shop = new Shop(shopName);
        Shop newShop = shopRepository.save(shop);
        return ResponseEntity.status(HttpStatus.OK).body(newShop);
    }

    @PostMapping("/addManager/{shopId}")
    public HttpEntity<Shop> addManager(@PathVariable long id, @RequestBody Staff staff) {
        Optional<Shop> shop = shopRepository.findById(id);
        if (shop.isPresent()) {
            if (shop.get().getManager() == null) {
                Manager manager = new Manager(staff.getId(), id);
                shop.get().setManager(manager);
                shopRepository.save(shop.get());
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(shop.get());
    }


    @PatchMapping("/editName/{shopId}")
    public HttpEntity<String> editShopName(@PathVariable long shopId, @RequestParam String name) {

        Optional<Shop> shop = shopRepository.findById(shopId);

        if (shop.isPresent()) {
            shop.get().setName(name);
        }
        shopRepository.save(shop.get());
        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("แก้ไขชื่อร้านสำเร็จ"));
    }

    @DeleteMapping(path = "/delete/{id}")
    public HttpEntity<String> delete(@PathVariable long id) {
        Optional<Shop> shop = shopRepository.findById(id);

        if (shop.isPresent()) {
            shopRepository.deleteById(id);
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.valueOf("ไม่มีไอ้สัส"));
        }

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("ลบ id " + String.valueOf(id) + " สำเร็จ"));
    }



}
