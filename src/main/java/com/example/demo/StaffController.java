package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ManagerRepository managerRepository;

    @GetMapping("/get")
    public HttpEntity<List<Staff>> getAll() {
        List<Staff> staffs = staffRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(staffs);
    }

    @PostMapping("/add")
    public HttpEntity<Staff> addStaff(@RequestBody Staff staff) {
        if (staff.getRole().equals("MANAGER")) {
            managerRepository.save(new Manager(staff.getId(), staff.getShopId()));
        }
        Staff newStaff = staffRepository.save(staff);
        return ResponseEntity.status(HttpStatus.OK).body(newStaff);
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<String> editStaff(@PathVariable long id,
                                       @RequestParam Optional<String> name,
                                       @RequestParam Optional<Long> shopId,
                                       @RequestParam Optional<String> role) {

        Optional<Staff> staff = staffRepository.findById(id);

        if (staff.isPresent()) {
            if (name.isPresent()) staff.get().setName(name.get());
            if (shopId.isPresent()) staff.get().setShopId(shopId.get());
            if (role.isPresent()) staff.get().setRole(role.get());
        }

        staffRepository.save(staff.get());

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("แก้ไขสำเร็จ"));
    }

    @DeleteMapping(path = "/delete/{id}")
    public HttpEntity<String> delete(@PathVariable long id) {
        Optional<Staff> staff = staffRepository.findById(id);

        if (staff.isPresent()) {
            staffRepository.deleteById(id);
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.valueOf("ไม่มีให้ลบจ้าาา"));
        }

        return ResponseEntity.status(HttpStatus.OK).body(String.valueOf("ลบ id " + String.valueOf(id) + " สำเร็จ"));
    }


}
